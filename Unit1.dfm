object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Change pass SQLite'
  ClientHeight = 201
  ClientWidth = 283
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  DesignSize = (
    283
    201)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 39
    Width = 265
    Height = 40
    Anchors = [akLeft, akTop, akRight]
    Caption = 'label'
    ParentShowHint = False
    ShowHint = True
    WordWrap = True
  end
  object Label2: TLabel
    Left = 8
    Top = 117
    Width = 71
    Height = 13
    Caption = 'Encryption key'
  end
  object Label3: TLabel
    Left = 8
    Top = 144
    Width = 95
    Height = 13
    Caption = 'New encryption key'
  end
  object Label4: TLabel
    Left = 8
    Top = 90
    Width = 98
    Height = 13
    Caption = 'Encryption algorithm'
  end
  object Button1: TButton
    Left = 8
    Top = 8
    Width = 145
    Height = 25
    Caption = 'Open SQLite database file'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Edit1: TEdit
    Left = 112
    Top = 114
    Width = 161
    Height = 21
    TabOrder = 1
  end
  object Edit2: TEdit
    Left = 112
    Top = 141
    Width = 161
    Height = 21
    TabOrder = 2
  end
  object Button2: TButton
    Left = 168
    Top = 168
    Width = 105
    Height = 25
    Caption = 'Change password'
    Enabled = False
    TabOrder = 3
    OnClick = Button2Click
  end
  object ComboBox1: TComboBox
    Left = 112
    Top = 87
    Width = 161
    Height = 21
    TabOrder = 4
    Text = 'leAES256'
    Items.Strings = (
      'leTripleDES'
      'leBlowfish'
      'leAES128'
      'leAES192'
      'leAES256'
      'leCast128'
      'leRC4')
  end
  object Button3: TButton
    Left = 87
    Top = 168
    Width = 75
    Height = 25
    Caption = 'Close'
    TabOrder = 5
    OnClick = Button3Click
  end
  object FileOpenDialog1: TFileOpenDialog
    FavoriteLinks = <>
    FileTypes = <
      item
        DisplayName = 'SQLiteDatabaseFile'
        FileMask = '*.db3; *.db'
      end>
    Options = []
    OnFileOkClick = FileOpenDialog1FileOkClick
    Left = 115
    Top = 168
  end
  object SQLiteUniProvider1: TSQLiteUniProvider
    Left = 67
    Top = 168
  end
  object UniConnection1: TUniConnection
    ProviderName = 'SQLite'
    SpecificOptions.Strings = (
      'SQLite.EncryptionAlgorithm=leAES256')
    DefaultTransaction.DefaultConnection = UniConnection1
    Left = 19
    Top = 168
  end
end
