# README #

Change SQLite database encryption key

Смена или сброс криптоключа базы данных SQLite

### Компоненты необходимые для сборки ###

* UniDac http://www.devart.com/unidac/
* SQLite + UniDac для x64 систем http://www.devart.com/unidac/docs/index.html?sqliteprov_article.htm

### Компоненты необходимые для работы готового ПО ###

Работоспособность ПО на различных ОС не тестировалось, возможно рядом с исполняемым файлом нужно будет положить библиотеку SQLite (sqlite3.dll), лежит рядом с исходниками


### Получение исходников ###

https://bitbucket.org/armani_zh/decryptsqlitedatabase/downloads

git clone https://armani_zh@bitbucket.org/armani_zh/decryptsqlitedatabase.git

### Получение готового приложения ###

С библиотекой SQLite

https://bitbucket.org/armani_zh/decryptsqlitedatabase/downloads/ChangePassSQLite(with%20sqlite%20lib).zip

Без библиотек

https://bitbucket.org/armani_zh/decryptsqlitedatabase/downloads/ChangePassSQLite.zip

### Вопросы, пожелания и замечания ###

* Баги и ошибки оставляйте в багтрекере
* По другим вопросам обращаться [armani.zh@gmail.com](mailto:armani.zh@gmail.com)