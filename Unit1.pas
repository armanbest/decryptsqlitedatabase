unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Data.DB, DBAccess, Uni,
  UniProvider, SQLiteUniProvider, Vcl.ComCtrls;

type
  TForm1 = class(TForm)
    FileOpenDialog1: TFileOpenDialog;
    Button1: TButton;
    Label1: TLabel;
    Edit1: TEdit;
    Label2: TLabel;
    Label3: TLabel;
    Edit2: TEdit;
    Button2: TButton;
    SQLiteUniProvider1: TSQLiteUniProvider;
    UniConnection1: TUniConnection;
    ComboBox1: TComboBox;
    Label4: TLabel;
    Button3: TButton;
    procedure Button1Click(Sender: TObject);
    procedure FileOpenDialog1FileOkClick(Sender: TObject;
      var CanClose: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    filePath :string;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
begin
  if FileOpenDialog1.Execute then begin
     filePath := FileOpenDialog1.FileName;
     Label1.Caption := filePath;
     Label1.Hint := filePath;
  end;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  UniConnection1.Database := filePath;
  UniConnection1.SpecificOptions.Values['ForceCreateDatabase'] := 'True';
  UniConnection1.SpecificOptions.Values['Direct'] := 'False';
  UniConnection1.SpecificOptions.Values['EncryptionAlgorithm'] := ComboBox1.Text;
  UniConnection1.SpecificOptions.Values['EncryptionKey'] := Edit1.Text;

  try
    UniConnection1.Open;
    TLiteUtils.EncryptDatabase(UniConnection1, Edit2.Text);
    UniConnection1.Close;
    ShowMessage('Encryption key changed!');
  Except
    ShowMessage('Error (invalid algorithm or key)!');
  end;
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
  Close;
end;

procedure TForm1.FileOpenDialog1FileOkClick(Sender: TObject;
  var CanClose: Boolean);
begin
  Button2.Enabled := True;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
Label1.Caption := '';
end;

end.
